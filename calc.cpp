#include <iostream>
#include <stack>
#include <vector>
#include <cstdlib>
#include <string>
#include <math.h>

using namespace std;

int priorytet (char znak); //nadawanie priorytetu operatorom
int PrintVector(vector<int> w);
void PrintStack(stack<char> &s);
void PrintStos(stack<char> &s);
int change (string wyrazenie);//zamiana wyrazenia na ONP
float Counting (int a, char c, int b);
void Result (vector<int> &w);//obliczanie wartosci wyrazenia


int main ()

{
    string wyrazenie;
    cout<<"Kalkulator posiada funkcje: \n+ dodawania\n- odejmowania\n* mnozenia\n/ dzielenia\n^ potegowania\n"<<endl;
    cout<<"Wersja 1.1 obsluguje liczby calkowite\n Przed nawiasem otwierajacym musi znalezc sie jeden z w.w. operatorow \nNie obsluguje liczb ujemnych"<<endl;
    //jezeli chcesz obliczyc wieksze liczby zmien typ zmiennych!
    cin>>wyrazenie;

    change(wyrazenie);
    return 0;

}

int priorytet (char znak)
{
    if (znak=='*' || znak=='/' || znak=='^') return 2;
    else if (znak=='+' || znak=='-' || znak==')') return 1;
    else if (znak== '(') return 0;
    else return -1;
}

int PrintVector(vector<int> w)
{
    cout<<"Wyjscie: ";
    for(int x = 0; x<w.size(); x++)
    {
        if((w[x])<0)
            cout<<((char)(w[x]*-1))<<" ";

        else
            cout<<(w[x])<<" ";
    }
    cout<<endl;
    return 0;
}

void PrintStack(stack<char> &s)
{
    if(s.empty())
    {
        return;
    }
    char x= s.top();
    s.pop();
    PrintStack(s);
    s.push(x);
    cout << x << " ";
}

void PrintStos(stack<char> &s)
{
    cout<<"Stos: ";
    PrintStack(s);
    cout<<endl;
}

int change (string wyrazenie)
{
    stack<char>stos;
    vector<int>wektor;
    vector<int>liczba;

    for (int x=0; x<wyrazenie.size(); x++)
    {
        //cout<<"Przeczytaj wejscie: "<<wyrazenie[x]<<endl;
        if (wyrazenie[x]>='0' && wyrazenie[x]<='9')//jezeli liczba wysylamy ja do wektora
    {
       liczba.push_back(wyrazenie[x]-'0');

            int suma_cyfr=0;
            int mnoznik=1;

        if ((x+1)==wyrazenie.size() || (wyrazenie[x+1]<'0' || wyrazenie[x]>'9'))
            {
                for(int i=liczba.size()-1; i>=0; i--)
                {
                    suma_cyfr+=(liczba[i]*mnoznik);
                    mnoznik*=10;
                }
                liczba.clear();
                //cout<<"Suma_cyfr= "<<suma_cyfr<<endl;
                wektor.push_back(suma_cyfr);
    }}

    else if (wyrazenie[x]=='(')
    {
        stos.push(wyrazenie[x]);
    }

    else if (wyrazenie[x]=='+' || wyrazenie[x]=='-'|| wyrazenie[x]=='*'||

             wyrazenie[x]=='/'|| wyrazenie[x]=='^')//jezeli operator wysylamy go na stos
    {
        if (stos.empty() || priorytet(wyrazenie[x]) > priorytet(stos.top()))

        {
            stos.push(wyrazenie[x]);
        }
        else
        {
            while (!stos.empty()&&(wyrazenie[x]!='^' && priorytet(wyrazenie[x])<= priorytet(stos.top())) ||
                    (wyrazenie[x])=='^' && priorytet(wyrazenie[x])<priorytet(stos.top()))
            {
                wektor.push_back(stos.top()*-1);
                stos.pop();
            }
            stos.push(wyrazenie[x]);
        }
    }
    else if (wyrazenie[x]==')')//jezeli ) odczytujemy i wysylamy wszystkie operatory ze stosu do wektora az do (
    {
        while(stos.top()!='(')
        {
            wektor.push_back(stos.top()*-1);
            stos.pop();
        }
        stos.pop();
    }
    //TESTY UNIT//////////////////////////////////////
    //PrintVector(wektor);
    //PrintStos(stos);
    //cout<<"------------------------------"<<endl;

}//koniec petli for
while(!stos.empty()) //oprozniamy stos z pozostalych operatorow i wpisujemy je do wektora
{
    if (stos.top()!='(')
    {
        wektor.push_back(stos.top()*-1);
    }
    stos.pop();

}
//TESTY UNIT//////////////////////////////////////
/*cout<<"wektor size = "<<wektor.size()<<endl;
cout<<"jest: ";
for (int i=0; i<wektor.size(); i++){
    if(wektor[i]>9)
    {
        cout<<((char)wektor[i])<<" ";
    }
    else
    {
        cout<<wektor[i]<<" ";
    }
}
cout<<endl;*/
Result (wektor);
return 0;
}

float Counting (float a, char c, float b)
{
    if (c=='+') return b+a;
    else if (c=='-') return b-a;
    else if (c=='*') return b*a;
    else if (c=='/') return b/a;
    else if (c=='^') return pow(b, a);
}

void Result (vector<int> &w)
{
    stack<float>stos;
    for (int x=0; x<w.size(); x++)
    {
        if (w[x]>=0)//jezeli liczba wysylamy ja na stos
        {
            stos.push(w[x]);
        }
        else
        {
            float a = stos.top();
            stos.pop();
            float b = stos.top();
            stos.pop();
            char o=((char)(w[x]*-1));
            stos.push(Counting(a, o, b));
        }
    }
    cout<<"Wynik: "<<stos.top()<<endl;
    stos.pop();
}